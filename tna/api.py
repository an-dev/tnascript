import json

from settings.base import DISCOVERY_BASE_URL

from urllib.request import urlopen

from tna.utils import INSUFFICIENT_INFO, NO_RECORD_FOUND, NO_CONTENT, OK


def handle_response(json_response):
    """
    Conditions are not mutually exclusive, hence the use of elif
    """
    if json_response.get('title'):
        response = json_response['title']
    elif json_response.get('scopeContent') and json_response['scopeContent'].get('description'):
        response = json_response['scopeContent']['description']
    elif json_response.get('reference'):
        response = json_response['reference']
    else:
        response = INSUFFICIENT_INFO
    
    return response


def run(record_id):
    url = f"{DISCOVERY_BASE_URL}/records/v1/details/{record_id}/"
    with urlopen(url) as response:
        read_response = response.read()
        result = read_response

        if response.status == OK:
            json_response = json.loads(read_response)
            result = handle_response(json_response)
        
        if response.status == NO_CONTENT:
            result = NO_RECORD_FOUND
        
        return result
