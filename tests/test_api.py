import json
import pathlib
import unittest

from tna import api
from tna.utils import INSUFFICIENT_INFO, NO_RECORD_FOUND


def load_fixture(filename):
    fpath = pathlib.Path(__file__).parent.joinpath(
        "fixtures", filename
    )
    with open(str(fpath)) as fo:
        return json.load(fo)


class TestHandleResponse(unittest.TestCase):
    def test_handle_response_success_returns_title(self):
        """
        if id is specified and the returned record’s `title` is NOT null
        display record's `title`
        """
        data = load_fixture('success_title.json')
        assert api.handle_response(data) == data['title']

    def test_handle_response_success_returns_description(self):
        """
        if id is specified and
        - the returned record’s `title` is null
        - the returned record’s `scopeContent.description` is NOT null
        display record's `scopeContent.description`
        """
        data = load_fixture('success_description.json')
        assert api.handle_response(data) == data['scopeContent']['description']

    def test_handle_response_success_returns_reference(self):
        """
        if id is specified and
        - the returned record’s `title` is null
        - the returned record’s `scopeContent.description` is null
        - the returned record’s `reference` is NOT null
        display record's `reference`
        """
        data = load_fixture('success_reference.json')
        assert api.handle_response(data) == data['reference']

    def test_handle_response_success_returns_insufficient_info(self):
        """
        if id is specified and
        - the returned record’s `title` is null
        - the returned record’s `scopeContent.description` is null
        - the returned record’s `reference` is null
        display `not sufficient information`
        """
        data = load_fixture('success_insufficient.json')
        assert api.handle_response(data) == INSUFFICIENT_INFO


class TestRun(unittest.TestCase):
    def test_api_run_invalid_record_id(self):
        assert api.run('random_id') == NO_RECORD_FOUND


if __name__ == "__main__":
    unittest.main()
