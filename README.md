# TnaScript

- [Description](#description)
- [Installation](#installation)
- [Notes](#notes)
- [References](#references)


## Description
Tnascript - minimal open Rest API data collector for the Discovery platform


## Installation
To install, clone the repository locally:

`git clone https://bitbucket.org/an-dev/tnascript/`

Run with Python3

`python3 main.py <record_id>`

e.g.: `python3 main.py 4b33f166-acec-4f99-9cfb-a5433edfe89e`

## Testing
```
python3 -m unittest tests/test_api.py
```


## Notes

A minimalistic, native approach was chosen to build the script, taking advantage of some of python3 built-in libraries,
so as to avoid using 3rd party dependencies:



**Argument handling**

[Argparse](https://docs.python.org/3/howto/argparse.html) was used as a personal preference and quick argument handling


**HTTP Requests**

In a commercial setting [urllib](https://docs.python.org/3/howto/urllib2.html) would have been replaced with [requests](https://requests.kennethreitz.org/en/master/), as the latter has an easier interface to call various HTTP methods and easier to test against (and mock)


**Testing**

As for testing, [pytest](https://docs.pytest.org/en/6.2.x/) would have been used in a commercial setting as it provides an easier interface for calling its methods, common testing patterns, factories and built in module discovery


**Formatting**

Python best practices along with PEP8 style guides were followed


**Missing reference property in record-id retrieval**

There might be a potential bug in the record retrieval, as the `reference` property seems to be missing when retrieving a single record (as opposed to retrieving a set of records via a keyword search)

```
the following api call http://discovery.nationalarchives.gov.uk/API/search/v1/records?sps.searchQuery=titanic 
will return a set of records containing the `reference` property, 
but retrieving a single entity (e.g.: 60c0a4f3-98d3-4ecb-96fe-c94a90aa5eb1) will not

```

A potential solution for this would be to attempt to retrieve the information making another api call and merging the objects, only to then attempt to find a valid property to output as a result.


**Output formatting**

The task instructions didn't specify how the output should be handled, so, in line with the minimalistic approach taken bulding the script, the results are printed "as-it-is"



## References
Web pages used/consulted while building the script
 
https://docs.python.org/3/howto/argparse.html

https://docs.python.org/3/library/unittest.html

https://docs.python.org/3/howto/urllib2.html

http://discovery.nationalarchives.gov.uk/API/sandbox/index#!/Records/Records_GetRecord
