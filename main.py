import argparse
from tna import api


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("record_id", help="record id from Discovery API")

    args = parser.parse_args()

    result = api.run(args.record_id)
    print(result)


if __name__ == "__main__":
    main()
